#! /bin/bash

# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

echo -n 'setting up jitsi ... '

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

mkdir -p icons
wget -qO icons/jitsi.png https://meet.jit.si/images/apple-touch-icon.png

cat << EOF > $HOME/bin/jitsi
#! /bin/bash
# firejail --x11=xephyr --profile=/etc/firejail/chromium.profile chromium --app=https://meet.jit.si
firejail --profile=/etc/firejail/chromium.profile chromium --app=https://meet.jit.si
EOF
chmod 0700 $HOME/bin/jitsi

cat << EOF > $HOME/.local/share/applications/Jitsi.desktop
[Desktop Entry]
Name=Jitsi
Exec=$HOME/bin/jitsi
StartupNotify=true
Terminal=false
Type=Application
Icon=$SCRIPTPATH/icons/jitsi.png
EOF

echo 'OK'
