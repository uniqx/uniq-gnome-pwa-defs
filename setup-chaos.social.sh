#! /bin/bash

# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

echo -n 'setting up chaos.social ... '

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

mkdir -p icons
wget -qO icons/chaos.social.ico https://chaos.social/favicon.ico

cat << EOF > $HOME/bin/chaos.social
#! /bin/bash
firejail --profile=/etc/firejail/firefox-esr.profile firefox -P chaos.social
EOF
chmod 0700 $HOME/bin/chaos.social

cat << EOF > ~/.local/share/applications/chaos.social.desktop
[Desktop Entry]
Name=chaos.social
Exec=$HOME/bin/chaos.social
StartupNotify=true
Terminal=false
Type=Application
Icon=$SCRIPTPATH/icons/chaos.social.ico
EOF

echo 'OK'
