#! /bin/bash

# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

$SCRIPTPATH/setup-chaos.social.sh
$SCRIPTPATH/setup-jitsi.sh
$SCRIPTPATH/setup-pixelfed.social.sh
