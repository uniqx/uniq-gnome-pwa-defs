#! /bin/bash

# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

echo -n 'setting up pixelfed.social ... '

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

mkdir -p icons
wget -qO icons/pixelfed.social.png 'https://pixelfed.social/img/favicon.png?v=2'

cat << EOF > $HOME/bin/pixelfed.social
#! /bin/bash
firejail --profile=/etc/firejail/chromium.profile chromium --app=https://pixelfed.social
EOF
chmod 0700 $HOME/bin/pixelfed.social

cat << EOF > ~/.local/share/applications/pixelfed.social.desktop
[Desktop Entry]
Name=pixelfed.social
Exec=$HOME/bin/pixelfed.social
StartupNotify=true
Terminal=false
Type=Application
Icon=$SCRIPTPATH/icons/pixelfed.social.png
EOF

echo 'OK'
