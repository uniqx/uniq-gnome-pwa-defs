<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# uniq-gnome-pwa-defs

This is how I embed some websites into Gnome as a PWA.

## dev notes

* dependencies currently not tracked
* check licensing `reuse lint`
